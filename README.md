Java Advanced, 3 days
====

Welcome to this course.

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to demo projects

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation

    git clone https://gitlab.com/ribomation-courses/java/java-advanced.git
    cd java-advanced

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

Java
----
In order to do the programming exercises of the course,
you need to have Java JDK version 8 or later installed.
* [Java JDK Download](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

You also need a decent Java IDE.
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download)

Using SDKMAN in GIT Bash
----

The recommended way of installing JDK tools is to use SDKMAN.
However, it only works in a BASH (or *NIX like environment). For running it on
Windows, you need to use CygWin or MinGW. The latter is part of of GIT for Windows,
if you select that option during the install. Using GIT BASH, will be our platform
of choice during the course.

For SDKMAN to install, you need add more programs to MinGW. You can find GNU Win
executables at the [GNU Win Repo](https://sourceforge.net/projects/gnuwin32/files/).
Locate and download the [ZIP bundle](https://sourceforge.net/projects/gnuwin32/files/zip/3.0/zip-3.0-bin.zip/download).
Then unpack the content into the /usr/bin directory of the GIT MinGW installation. For example

    C:\Program Files\Git\usr\bin
It's also recommended to download/install the [tree](https://sourceforge.net/projects/gnuwin32/files/tree/1.5.2.2/tree-1.5.2.2-bin.zip/download) command.

Then, proceed and [install SDKMAN](http://sdkman.io/install.html)

    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    sdk version

When SDKMAN is working, you can easily
[install JDK and other JVM tools](http://sdkman.io/usage.html)

    sdk install java 9.0.4-oracle

If you quickly want to list all available tools, in a more compact way than
`sdk list`, use the following one-liner

    sdk list | awk 'BEGIN {RS="[-]+\n"; FS="\n"} {print $1}'

Links to Large Files
====
For some of the exercises, you might want to use a large input file. Here are some compressed large text files to use.
* [English Text, 100MB (_38MB compressed_)](https://docs.ribomation.se/java/java-8/english.100MB.gz)
* [English Text, 1024MB (_396MB compressed_)](https://docs.ribomation.se/java/java-8/english.1024MB.gz)
* [Climate Data, 2,5GB (_432MB compressed_)](https://docs.ribomation.se/java/java-8/climate-data.txt.gz)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
