package ribomation.java_intermediate.security.policies;

import java.security.Permission;

public class ReadSysPropsWithCustomSecMgr {
    public static void main(String[] args) {
        System.setSecurityManager(new SecurityManager(){
            @Override
            public void checkPermission(Permission perm) {
                System.out.println("GRANTED: "+ perm);
            }
        });

        ReadSysPropsWithCustomSecMgr app = new ReadSysPropsWithCustomSecMgr();
        app.sysprop("os.name");
        app.sysprop("java.version");
        app.sysprop("user.home");
        app.sysprop("java.home");
    }

    void sysprop(String name) {
        try {
            System.out.printf("Try '%s'%n", name);
            System.out.printf("sysprop[%s] = '%s'%n", name, System.getProperty(name, "*** not found"));
        } catch (Exception e) {
            System.out.printf("error: %s%n", e);
        }
    }
}
