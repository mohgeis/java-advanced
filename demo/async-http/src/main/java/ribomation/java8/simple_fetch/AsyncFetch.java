package ribomation.java8.simple_fetch;

import org.asynchttpclient.AsyncCompletionHandler;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class AsyncFetch extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new AsyncFetch().run();
    }

    void run() throws IOException, ExecutionException, InterruptedException {
        try (AsyncHttpClient http = new DefaultAsyncHttpClient()) {
            http.prepareGet(url).execute(new AsyncCompletionHandler<Void>() {
                @Override
                public Void onCompleted(Response response) {
                    print(response);
                    return null;
                }
            })
            .get();
        }
    }

}
