package ribomation.java8.extracting_links;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.CompletableFuture;


public class BaseStuff {
    String base   = "https://www.ribomation.se";
    String course = "/java/java-8.html";

    CompletableFuture<Document> fetchDoc(URL url) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return Jsoup.connect(url.toString()).get();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    URL mkUrl(String uri) {
        try {
            return new URL(base + uri);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
