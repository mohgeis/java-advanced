package ribomation.java8.extracting_links;
import java.util.stream.Collectors;

public class RelatedCourses extends BaseStuff {
    public static void main(String[] args) {
        RelatedCourses app = new RelatedCourses();
        app.run();
    }

    void run() {
        fetchDoc(mkUrl(course))
                .thenApply(doc -> doc.select(".schedule a"))
                .thenApply(nodes -> nodes.stream()
                        .map(node -> node.attr("href"))
                        .map(uri -> base + uri)
                        .collect(Collectors.toList()))
                .thenAccept(links -> links.forEach(System.out::println))
                .join();
    }

}
