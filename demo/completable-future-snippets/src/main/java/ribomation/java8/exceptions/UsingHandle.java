package ribomation.java8.exceptions;

import java.util.concurrent.CompletableFuture;

public class UsingHandle extends BaseStuff {
    public static void main(String[] args) {
        new UsingHandle().run();
    }

    CompletableFuture<String> chain(int value) {
        return delayedValue(value, 1)
                .thenApply(n -> {
                    if (n % 2 == 1) throw new IllegalArgumentException("ooops");
                    return n;
                })
                .handle((ok, err) -> {
                    if (ok != null && err == null) {
                        return ok;
                    } else if (ok == null && err != null) {
                        System.out.printf("*** %s%n", err.getMessage());
                    } else {
                        System.err.println("Fatal error");
                        System.exit(666);
                    }
                    return -1;
                })
                .thenApply(n -> 10 * n)
                .thenApply(n -> String.format("value = %d", n))
                ;
    }

}
