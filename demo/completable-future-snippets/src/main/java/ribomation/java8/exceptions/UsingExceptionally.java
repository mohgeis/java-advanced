package ribomation.java8.exceptions;
import java.util.concurrent.CompletableFuture;

public class UsingExceptionally extends BaseStuff {
    public static void main(String[] args) {
        new UsingExceptionally().run();
    }

    CompletableFuture<String> chain(int value) {
        return delayedValue(value, 1)
                .thenApply(n -> {
                    if (n % 2 == 1) throw new IllegalArgumentException("ooops");
                    return n;
                })
                .exceptionally(err -> {
                    System.out.printf("*** %s%n", err.getMessage());
                    return -1;
                })
                .thenApply(n -> 10 * n)
                .thenApply(n -> String.format("value = %d", n))
                ;
    }

}
