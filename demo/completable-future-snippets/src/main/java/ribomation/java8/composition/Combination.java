package ribomation.java8.composition;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;

public class Combination extends BaseStuff {
    public static void main(String[] args) {
        new Combination().run();
    }

    void run() {
        long                       start = System.nanoTime();
        CompletableFuture<Integer> g     = delayedValue(20);
        CompletableFuture<Integer> h     = delayedValue(22);
        BiFunction<Integer, Integer, String> f = (lhs, rhs) ->
                String.format("%d + %d = %d", lhs, rhs, lhs + rhs);

        String result = g.thenCombine(h, f).join();
        System.out.printf("The answer is %s. Elapsed %.4f secs%n",
                result,
                (System.nanoTime() - start) * 1E-9);
    }

}
