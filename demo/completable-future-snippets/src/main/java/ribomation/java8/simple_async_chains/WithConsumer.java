package ribomation.java8.simple_async_chains;

public class WithConsumer extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new WithConsumer().run();
    }

    void run() {
        long start = System.nanoTime();
        delayedValue(42, 2)
                .thenAccept(answer ->
                        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                                answer, (System.nanoTime() - start) * 1E-9)
                )
                .join();
    }

}
