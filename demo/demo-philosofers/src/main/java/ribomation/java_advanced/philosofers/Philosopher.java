package ribomation.java_advanced.philosofers;

public class Philosopher extends Thread {
    private final int       id;
    private       ChopStick left;
    private       ChopStick right;

    public Philosopher(int id, ChopStick left, ChopStick right) {
        this.id = id;
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        while (true) {
            try {
                log("THINKING");
                log("HUNGRY");
                log("Grabbing LEFT chopstick");
                left.acquire();
                log("Grabbing RIGHT chopstick");
                right.acquire();
                log("EATING");
                log("Dropping right chopstick");
                right.release();
                log("Dropping left chopstick");
                left.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void log(String msg) {
        String txt = String.format("%s[P-%d] %s%n", TAB(id), id, msg);
        System.out.print(txt);
    }

    private static String TAB(int n) {
        StringBuilder buf = new StringBuilder();
        while (n-- > 0) buf.append("          ");
        return buf.toString();
    }
}
