The Dining Philosofers Threads Demo
====

How to run the demo
----

### *NIX

    ./gradlew deadlock
    ./gradlew deadlockFree

### Windows

    ./gradlew.bat deadlock
    ./gradlew.bat deadlockFree

N.B.
----
The build is based on Gradle and its wrapper.
There is no need to install Gradle, just use
the provided wrapper, as shown above.
