package ribomation.java_advanced.atm;

public class Updater extends Thread {
    private final Account account;
    private final int     N;

    public Updater(int id, Account account, int n) {
        super("Updater-" + id);
        this.account = account;
        this.N = n;
    }

    @Override
    public void run() {
        final int amount = 100;
        System.out.printf("[%s] started%n", getName());
        for (int k = 0; k < N; ++k) account.updateBalance(+amount);
        for (int k = 0; k < N; ++k) account.updateBalance(-amount);
        System.out.printf("[%s] done%n", getName());
    }
}
