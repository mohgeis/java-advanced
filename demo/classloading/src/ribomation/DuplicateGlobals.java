package ribomation;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class DuplicateGlobals {
    public static void main(String[] args) throws Exception {
        new DuplicateGlobals().run();
    }

    Path           classesDir;
    Path           deployDir;
    String         targetClass;
    String         targetFile;

    DuplicateGlobals() throws IOException {
        classesDir  = Paths.get("./build/classes/main");
        deployDir   = Files.createTempDirectory("duplicate-globals");
        targetClass = getClass().getPackage().getName() + ".Target";
        targetFile  = targetClass.replace('.', '/') + ".class";
    }

    void run() throws Exception {
        moveTarget();
        List<Runnable> objects = createObjects();
        dump(objects);
        System.out.println("--- shake() ---");
        shake(objects);
        System.out.println("--- dump() ---");
        dump(objects);
    }

    void moveTarget() throws IOException {
        Path source = Paths.get(classesDir.toString(), targetFile);
        Path target = Paths.get(deployDir.toString(), targetFile);
        Files.createDirectories(target.getParent());
        Files.move(source, target, StandardCopyOption.REPLACE_EXISTING);
        System.out.printf("Moved to %s%n", target);
    }

    List<Runnable> createObjects() throws Exception {
        ClassLoader    ld1     = createClassLoader();
        ClassLoader    ld2     = createClassLoader();
        List<Runnable> objects = new ArrayList<>();
        objects.add((Runnable) createObject(ld1, targetClass, "A1"));
        objects.add((Runnable) createObject(ld1, targetClass, "A2"));
        objects.add((Runnable) createObject(ld2, targetClass, "B1"));
        objects.add((Runnable) createObject(ld2, targetClass, "B2"));
        return objects;
    }

    Object createObject(ClassLoader loader, String classname, String id) throws Exception {
        Class<?>       cls  = loader.loadClass(classname);
        Constructor<?> cons = cls.getConstructor(String.class);
        Object         obj  = cons.newInstance(id);
        System.out.printf("Created %s%n", obj);
        return obj;
    }

    ClassLoader createClassLoader() throws MalformedURLException {
        URL[] dirs = new URL[]{deployDir.toUri().toURL()};
        return new URLClassLoader(dirs);
    }

    void dump(List<Runnable> objs) {
        for (Runnable obj : objs) System.out.printf("** %s%n", obj);
    }

    void shake(List<Runnable> objs) {
        for (Runnable obj : objs) obj.run();
    }
}
