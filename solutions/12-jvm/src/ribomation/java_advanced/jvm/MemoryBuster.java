package ribomation.java_advanced.jvm;


import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MemoryBuster {
    static final String CHUNK = "ABC";
    static final double MB    = 1024 * 1024.0;

    public static void main(String[] args) {
        MemoryBuster app = new MemoryBuster();
        app.run();
    }

    List objs = new LinkedList();

    void run() {
        Random r = new Random();
        while (true) {
            objs.add(mk(1 + r.nextInt(1000)));
            memory();
            sleep(500);
        }
    }

    void memory() {
        System.out.printf("MEMORY: use=%.3fMB, free=%.3fMB, total=%.3fMB, max=%.3fMB%n",
                (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / MB,
                Runtime.getRuntime().freeMemory() / MB,
                Runtime.getRuntime().totalMemory() / MB,
                Runtime.getRuntime().maxMemory() / MB
        );
    }

    String mk(int len) {
        StringBuilder buf = new StringBuilder();
        while (--len > 0) buf.append(CHUNK);
        return buf.toString();
    }

    void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ignore) {
        }
    }

}
