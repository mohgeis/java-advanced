public class App {
    
    public static void main(String[] args) {
        System.out.println("[main] enter");
        App app = new App();
        System.out.printf("app: %s%n", app);
        
        App app2 = new App();
        System.out.printf("app2: %s%n", app2);
        
        System.out.printf("app==app: %s%n", app.equals(app2));
        System.out.println("[main] exit");
    }
    
}