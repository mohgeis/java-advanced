#!/usr/bin/env bash

MAIN=java_advanced.sandbox.AccessDummyFile
CLS=./classes
CP="-cp $CLS"

echo "*** COMPILE"
mkdir -p $CLS
SRC=`echo $MAIN | tr . / | sed 's/^\(.*\)$/src\/\1.java/'`
(set -x; javac -d $CLS $SRC )

echo "*** RUN without SecurityManager"
(set -x; java $CP $MAIN )

echo ''
echo ''
echo "*** RUN with SecurityManager and default policy"
(set -x; java $CP -Djava.security.manager $MAIN )

echo ''
echo ''
echo "*** RUN with SecurityManager and custom policy"
(set -x; java $CP -Djava.security.manager -Djava.security.policy=dummy.policy $MAIN )




