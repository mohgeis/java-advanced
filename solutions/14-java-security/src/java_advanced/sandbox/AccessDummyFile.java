package java_advanced.sandbox;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.AccessControlException;
import java.util.Date;

public class AccessDummyFile {
    public static void main(String[] args) {
        AccessDummyFile app = new AccessDummyFile();
        app.read("./data/dummy.txt");
        app.write("./data/dummy-2.txt");
    }

    void read(String filename) {
        System.out.printf("--- READ %s ---%n", filename);
        try (BufferedReader in = new BufferedReader(new FileReader(filename))) {
            in.lines().forEach(System.out::println);
        } catch (FileNotFoundException e) {
            System.out.printf("Cannot open file: %s (%s)%n", filename, e);
        } catch (IOException e) {
            System.out.printf("Cannot read file: %s (%s)%n", filename, e);
        } catch (AccessControlException e) {
            System.out.printf("Sandbox error file: %s (%s)%n", filename, e);
        }
    }

    void write(String filename) {
        System.out.printf("--- WRITE %s ---%n", filename);
        try (PrintWriter out = new PrintWriter(new FileWriter(filename, true))) {
            out.printf("NEW LINE: %s %s%n", "Tjenare", new Date());
            System.out.printf("Written: %s%n", filename);
        } catch (FileNotFoundException e) {
            System.out.printf("Cannot open file: %s (%s)%n", filename, e);
        } catch (IOException e) {
            System.out.printf("Cannot write file: %s (%s)%n", filename, e);
        } catch (AccessControlException e) {
            System.out.printf("Sandbox error file: %s (%s)%n", filename, e);
        }
    }

}
