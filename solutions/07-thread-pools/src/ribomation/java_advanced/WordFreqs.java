package ribomation.java_advanced;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.concurrent.*;

public class WordFreqs {
    public static void main(String[] args) throws Exception {
        new WordFreqs().run(args);
    }

    void run(String[] args) throws Exception {
        String file     = "/shakespeare.txt";
        int    chunk    = 5 * 1024;
        int    minSize  = 5;
        int    maxWords = 100;

        for (String arg : args) {
            if (arg.startsWith("--file=")) {
                file = arg.substring(7);
            } else if (arg.startsWith("--chunk=")) {
                chunk = Integer.parseInt(arg.substring(8)) * 1024;
            } else if (arg.startsWith("--min=")) {
                minSize = Integer.parseInt(arg.substring(6));
            } else if (arg.startsWith("--max=")) {
                maxWords = Integer.parseInt(arg.substring(6));
            }
        }

        run(file, chunk, minSize, maxWords);
    }

    void run(String bookPath, int chunkSize, int minWordSize, int maxWords) throws Exception {
        InputStream                          is        = openStream(bookPath);
        List<Callable<Map<String, Integer>>> tasks     = createTasks(minWordSize, chunkSize, new InputStreamReader(is));
        ExecutorService                      pool      = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        List<Future<Map<String, Integer>>>   results   = pool.invokeAll(tasks);
        Map<String, Integer>                 wordFreqs = aggregate(results);
        print(maxWords, wordFreqs);
        shutdown(pool);
    }

    InputStream openStream(String bookPath) {
        InputStream is = getClass().getResourceAsStream(bookPath);
        if (is == null) {
            throw new IllegalArgumentException("cannot open resoruce: " + bookPath);
        }
        System.out.printf("Opened file %s%n", bookPath);
        return is;
    }

    List<Callable<Map<String, Integer>>> createTasks(int minWordSize, int chunkSize, Reader in) throws IOException {
        List<Callable<Map<String, Integer>>> tasks = new ArrayList<>();
        for (Callable<Map<String, Integer>> task; ((task = createTask(minWordSize, chunkSize, in)) != null); ) {
            tasks.add(task);
        }
        System.out.printf("Created %d tasks with chunk size of %d chars%n", tasks.size(), chunkSize);
        return tasks;
    }

    Callable<Map<String, Integer>> createTask(int minWordSize, int chunkSize, Reader in) throws IOException {
        char[] buf      = new char[chunkSize];
        int    numBytes = in.read(buf);
        if (numBytes < 0) return null;

        return () -> {
            String   payload = new String(buf, 0, numBytes);
            String[] words   = payload.split("[^a-zA-Z]+");

            Map<String, Integer> freqs = new HashMap<>();
            for (String word : words) {
                if (word.length() >= minWordSize) {
                    word = word.toLowerCase();
                    freqs.put(word, 1 + freqs.getOrDefault(word, 0));
                }
            }
            return freqs;
        };
    }

    Map<String, Integer> aggregate(List<Future<Map<String, Integer>>> results) throws Exception {
        Map<String, Integer> total = new HashMap<>();
        for (Future<Map<String, Integer>> r : results) {
            for (Map.Entry<String, Integer> p : r.get().entrySet()) {
                String word = p.getKey();
                int    freq = p.getValue();
                total.put(word, freq + total.getOrDefault(word, 0));
            }
        }
        return total;
    }

    void print(int max, Map<String, Integer> f) {
        System.out.printf("The %d most frequent words%n", max);
        f.entrySet().stream()
                .sorted((left, right) -> Integer.compare(right.getValue(), left.getValue()))
                .limit(max)
                .forEach((p) -> {
                    System.out.printf("%12s: %d%n", p.getKey(), p.getValue());
                });
    }

    void shutdown(ExecutorService pool) {
        pool.shutdown();
        try {
            if (pool.awaitTermination(1, TimeUnit.MINUTES)) return;
        } catch (InterruptedException ignore) {
        }
        pool.shutdownNow();
    }
}
