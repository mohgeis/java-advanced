package ribomation.java_advanced;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;

public class DirSize {
    public static void main(String[] args) throws IOException {
        new DirSize().run(new File("../../../../../"));
    }

    void run(File dir) throws IOException {
        System.out.printf("Scanning dir %s...%n", dir);

        long   start  = System.nanoTime();
        Result result = compute(dir);
        long   end    = System.nanoTime();

        System.out.printf(Locale.ENGLISH,
                "DIR: %s has%n %,d files of a total of %,.3f GB (elapsed %.3f secs)%n",
                dir.getCanonicalPath(),
                result.numFiles,
                result.numBytes / (1024 * 1024 * 1024.0),
                (end - start) * 1E-9
        );
    }

    Result compute(File dir) {
        return new ForkJoinPool().invoke(new ComputeSize(dir));
    }
}
