package ribomation.java_advanced.word_cloud_april;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;

public class WordCloud {

    public static void main(String[] args) throws IOException {
        WordCloud app = new WordCloud();
//        app.run("./data/musketeers.txt", 4, 100);
        app.run("./data/shakespeare.txt", 5, 100);
    }

    private void run(String filename, int minWordSize, int maxWords) throws IOException {
        final Pattern whiteSpace = Pattern.compile("[^a-zA-Z]+");

        List<Map.Entry<String, Long>> wordFreqs = Files.lines(Paths.get(filename))
                .flatMap(whiteSpace::splitAsStream)
                .filter(word -> word.length() >= minWordSize)
                .collect(groupingBy(String::toLowerCase, counting()))
                .entrySet().stream()
                .sorted((left, right) -> right.getValue().compareTo(left.getValue()))
                .limit(maxWords)
                .collect(Collectors.toList());

        long         maxFreq     = wordFreqs.get(0).getValue();
        long         minFreq     = wordFreqs.get(wordFreqs.size() - 1).getValue();
        int          maxFontSize = 120;
        int          minFontSize = 10;
        double       scale       = (double) (maxFontSize - minFontSize) / (maxFreq - minFreq);
        final Random r           = new Random();
        Supplier<String> mkColor = () -> String.format("#%02X%02X%02X",
                r.nextInt(256), r.nextInt(256), r.nextInt(256));

        String html = wordFreqs.stream()
                .map(pair -> String.format("<span style='font-size: %dpx; color: %s;'>%s</span>",
                        (int) (scale * pair.getValue()),
                        mkColor.get(),
                        pair.getKey()))
                .sorted((_left, _right) -> Math.random() < 0.5 ? -1 : +1)
                .collect(Collectors.joining(System.lineSeparator(), htmlStart, htmlEnd));

        try (BufferedWriter out = Files.newBufferedWriter(Paths.get("./word-cloud.html"))) {
            out.write(html);
        }
    }

    static final String htmlStart = "<html><body><h1>Word Cloud</h1>";
    static final String htmlEnd   = "</body></html>";

}