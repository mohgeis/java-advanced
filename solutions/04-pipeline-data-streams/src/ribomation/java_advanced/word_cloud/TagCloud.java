package ribomation.java_advanced.word_cloud;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.*;

public class TagCloud {
    public static void main(String[] args) throws Exception {
        new TagCloud()
                .run();
    }

    void run() throws IOException {
        run("/shakespeare.txt", 200, 4);
    }

    void run(String inputFile, int numWords, int minWordSize) throws IOException {
        long   start      = System.nanoTime();
        String outputFile = String.format("./out/%s.html", baseName(inputFile));
        String title      = capitalize(baseName(inputFile));
        int    minFont    = 12;
        int    maxFont    = 150;

        InputStream is = getClass().getResourceAsStream(inputFile);
        if (is == null) throw new RuntimeException("cannot open " + inputFile);

        List<Map.Entry<String, Long>> freqs   = computeFreq(is, minWordSize, numWords);
        List<String>                  tags    = makeTags(freqs, maxFont, minFont);
        long                          elapsed = System.nanoTime() - start;
        store(title, tags, outputFile, elapsed * 1E-9);
    }

    List<Map.Entry<String, Long>> computeFreq(InputStream is, int minWordSize, int maxNumWords) {
        return new BufferedReader(new InputStreamReader(is))
                .lines()
                .flatMap(Pattern.compile("[^a-zA-Z]+")::splitAsStream)
                .filter(word -> word.length() > minWordSize)
                .collect(groupingBy(String::toLowerCase, counting()))
                .entrySet().stream()
                .sorted(reverseOrder(comparingLong(Map.Entry::getValue)))
                .limit(maxNumWords)
                .collect(toList());
    }

    List<String> makeTags(List<Map.Entry<String, Long>> freqs, int maxFont, int minFont) {
        int          maxFreq = freqs.get(0).getValue().intValue();
        int          minFreq = freqs.get(freqs.size() - 1).getValue().intValue();
        double       scale   = (double) (maxFont - minFont) / (maxFreq - minFreq);
        final Random r       = new Random();
        Supplier<String> mkColor = () -> String.format("#%02X%02X%02X",
                r.nextInt(256), r.nextInt(256), r.nextInt(256));

        return freqs.stream()
                .map(wf -> String.format("<span style='font-size:%dpx; color:%s;'>%s</span>",
                        (int) (scale * wf.getValue()), mkColor.get(), wf.getKey()))
                .sorted((_a, _b) -> Math.random() < 0.5 ? -1 : +1)
                .collect(toList())
                ;
    }

    void store(String title, List<String> tags, String filename, double elapsed) throws IOException {
        try (BufferedWriter out = Files.newBufferedWriter(Paths.get(filename))) {
            out.write("<html><body>");
            out.write(String.format("<h1>%s Tag Cloud</h1>", title));
            out.write(String.format("<h3>Elapsed time %.3f seconds</h3>", elapsed));
            out.write(String.join(System.lineSeparator(), tags));
            out.write("</body></html>");
        } finally {
            System.out.printf("Written %s%n", filename);
        }
    }

    String baseName(String filename) {
        Path   path   = Paths.get(filename);
        String file   = path.getFileName().toString();
        int    extPos = file.lastIndexOf('.');
        return file.substring(0, extPos);
    }

    String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

}
