package ribomation.java_advanced.stock_ticker;

import java.util.Locale;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class StockTicker {
    private final ReadWriteLock rwl;
    private final String        name;
    private double price           = 0;
    private long   numTransactions = 0;

    static class Result {
        String name;
        double price;
        long   count;

        public Result(String name, double price, long count) {
            this.name = name;
            this.price = price;
            this.count = count;
        }

        @Override
        public String toString() {
            return String.format(Locale.ENGLISH, "%s/%.2f/%d", name, price, count);
        }
    }

    public StockTicker(String name) {
        this.name = name;
        rwl = new ReentrantReadWriteLock();
    }

    public Result value() {
        try {
            rwl.readLock().lock();
            return new Result(name, price, numTransactions);
        } finally {
            rwl.readLock().unlock();
        }
    }

    public Result update(float amount, int count) {
        try {
            rwl.writeLock().lock();
            price += amount;
            numTransactions += count;
            return new Result(name, price, numTransactions);
        } finally {
            rwl.writeLock().unlock();
        }
    }

}
