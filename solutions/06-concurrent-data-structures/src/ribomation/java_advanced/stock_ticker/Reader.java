package ribomation.java_advanced.stock_ticker;

import java.util.Random;

public class Reader extends Thread {
    private final int         id;
    private final StockTicker ticker;

    public Reader(int id, StockTicker ticker) {
        this.id = id;
        this.ticker = ticker;
    }

    @Override
    public void run() {
        Random r = new Random(System.nanoTime() % 37);
        try {
            do {
                StockTicker.Result value = ticker.value();
                System.out.printf("%" + (1 + id * 15) + "s[R-%d] %s%n", "", id, value);
                sleep(r.nextInt(100));
            } while (!interrupted());
        } catch (InterruptedException ignore) { }
        System.out.printf("%" + (1 + id * 15) + "s[R-%d] %s%n", "", id, "TERMINATED");
    }
}
