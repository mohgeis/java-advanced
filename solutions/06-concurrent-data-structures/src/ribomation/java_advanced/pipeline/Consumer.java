package ribomation.java_advanced.pipeline;

public class Consumer extends MessageThread<Long> {
    @Override
    public void run() {
        long msg;
        while ((msg = recv()) > 0) {
            System.out.printf("[consumer] %d%n", msg);
        }
    }
}
