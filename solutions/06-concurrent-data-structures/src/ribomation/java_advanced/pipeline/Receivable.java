package ribomation.java_advanced.pipeline;

public interface Receivable<MessageType> {
    void send(MessageType msg);
}
