package ribomation.java_advanced.pipeline;

import java.math.BigInteger;

public class Transformer extends MessageThread<Long> {
    private final Receivable<Long> next;

    public Transformer(Receivable<Long> next) {
        this.next = next;
    }

    @Override
    public void run() {
        long msg;
        while ((msg = recv()) > 0) {
            next.send(2 * msg);
        }
        next.send(-1L);
    }
}
