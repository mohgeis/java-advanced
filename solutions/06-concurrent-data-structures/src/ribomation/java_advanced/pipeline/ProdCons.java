package ribomation.java_advanced.pipeline;


public class ProdCons {
    public static void main(String[] args) throws Exception {
        new ProdCons().run(100);
    }
    void run(int n) throws InterruptedException {
        Consumer c = new Consumer();
        Producer p = new Producer(n, c);
        c.start();
        p.start();
        c.join();
    }
}
