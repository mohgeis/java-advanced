package ribomation.java_advanced.pipeline;


public class ProdTransCons {
    public static void main(String[] args) throws Exception {
        new ProdTransCons().run(50);
    }

    void run(int n) throws InterruptedException {
        Consumer    c = new Consumer();
        Transformer t = new Transformer(c);
        Producer    p = new Producer(n, t);
        c.start();
        t.start();
        p.start();
        c.join();
    }
}
