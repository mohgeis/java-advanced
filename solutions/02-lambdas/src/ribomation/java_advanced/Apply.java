package ribomation.java_advanced;

public interface Apply<T> {
    T transform(T x);
}
